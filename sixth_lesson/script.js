//Первое задание
function MakeHuman (name, age) {
    this.name = name;
    this.age = age;

}
const human = new MakeHuman('Anton', 24);
const human2 = new MakeHuman('Alisa', 25);
const human3 = new MakeHuman('Dima', 23);
const human4 = new MakeHuman('Dasha',33);
const human5 = new MakeHuman('Sasha', 41);
let humanArr = [human, human2, human3, human4, human5];

// С помощью Class

// class MakeHuman {
//     constructor (name, age) {
//         this.name = name;
//         this.age = age;
//     }
// }
// const humanArr = [
//     new MakeHuman('Anton', 24),
//     new MakeHuman('Alisa', 25),
//     new MakeHuman('Dima', 23),
//     new MakeHuman('Dasha',33),
//     new MakeHuman('Sasha', 41),
// ];

for (let i = 0; i < humanArr.length - 1; i++){ // - Логика для сортировки по увеличению
    for (let j = 0; j < humanArr.length - 1; j++) {
        if (humanArr[j].age > humanArr[j+1].age) {
            let oldest = humanArr[j].age;
            humanArr[j].age = humanArr[j+1].age;
            humanArr[j+1].age = oldest;
        }
    }
}
document.write(` Отсортированный массив : `)

for (let i = 0; i < humanArr.length; i++) {
    document.write(` ${humanArr[i].age} `)
}


// Второе задание
function Human (name, name2, age) {
    this.name = name;
    this.name2 = name2;
    this.age = age,
    Human.description = `Этот метод создает документ с данными про человека`;
}
Human.prototype.getData = function () {
    document.write(`Основная информация про человека : <br> имя : ${this.name} <br> фамилия : ${this.name2} <br> возраст : ${this.age} `)
}
Human.prototype.compatyHeWorksIn = function () {
    document.write(`Роботодатель - Prog_ua`);
}
Human.compareHumansAge = function () { // Присвоил данный метод к самой функции, так как такого рода метод будет принадлежать самой фунции , а каждому создаваемому объекту он не нужнен
    if (newHuman.age > newHuman2.age) {
        document.write(`${newHuman.name} старше чем ${newHuman2.name}`)
    } else {
        document.write(`${newHuman2.name} старше чем ${newHuman.name}`)
    }
}
const newHuman = new Human('Наташа', 'Калиновская', 28);
const newHuman2 = new Human('Андрей', 'Трегубов', 24);

// С помощью Class
// class Human {
//     constructor (name, name2, age) {
//         this.name = name;
//         this.name2 = name2;
//         this.age = age;
//     }
//     getData() {
//         document.write(`Основная информация про человека : <br> имя : ${this.name} <br> фамилия : ${this.name2} <br> возраст : ${this.age} `)
//     }
//     compatyHeWorksIn() {
//         document.write(`Роботодатель - Prog_ua`);
//     }
//     static compareHumansAge() {
//         if (newHuman.age > newHuman2.age) {
//             document.write(`${newHuman.name} старше чем ${newHuman2.name}`)
//         } else {
//             document.write(`${newHuman2.name} старше чем ${newHuman.name}`)
//         }
//     }
// }
// const newHuman = new Human('Наташа', 'Калиновская', 28);
// const newHuman2 = new Human('Андрей', 'Трегубов', 24);