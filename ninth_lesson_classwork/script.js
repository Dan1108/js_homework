let  interval, sec = 0, min = 0, hour = 0;
const get = id => document.querySelector(id);
const count = () => {
    sec++;
    if(sec == 60) {
        sec = 0;
        min++;
    }
    if(min == 60) {
        min = 0;
        hour++;

    }
    if(hour == 24) {
        hour = 0;
    }
    get("#output").textContent = `${hour}:${min}:${sec}`;
}
get("#output").textContent = `${hour}:${min}:${sec}`;

get("#startButton").onclick = () => {
    if(!interval) {
        interval = setInterval(count, 1000);
    }
}
get("#stopButton").onclick = () => {
    clearInterval(interval);
}
get("#resetButton").onclick = () => {
    clearInterval(interval);
    sec = 0;
    min = 0;
    hour = 0;
    get("#output").textContent = `${hour}:${min}:${sec}`;
}