const drawCircle = () => {
    const newBtn = document.createElement('input'); 
    newBtn.setAttribute("style", "background-color: green; width: 150px;");
    newBtn.setAttribute("type","button");
    newBtn.setAttribute("value", "Нарисовать");
    newBtn.setAttribute("onclick", "draw()");
    document.body.append(newBtn);
}
let wrapper = document.createElement('div');
document.body.append(wrapper);
wrapper.style.display = "flex";
wrapper.style.flexWrap = 'wrap';
wrapper.style.width = '100px';
const draw = () => {
    for(let i = 0; i < 10; i++) {
        for(let j = 0; j < 10; j++) {
            let circle = document.createElement('div');
            circle.style.cssText = ` width: 10px; height: 10px; border-radius: 50%; background-color: blue;`;
            circle.setAttribute("class", "circle");
            wrapper.append(circle);
            let circleHide = document.querySelectorAll('.circle');
            circleHide.forEach(item => {
                item.onclick = () => {
                    item.style.display = "none";
                }
            });
        } 
    }
}


