const curr = [{"ccy":"USD","base_ccy":"UAH","buy":"27.20000","sale":"27.60000"},{"ccy":"EUR","base_ccy":"UAH","buy":"31.95000","sale":"32.60000"},{"ccy":"RUR","base_ccy":"UAH","buy":"0.35900","sale":"0.38700"},{"ccy":"BTC","base_ccy":"USD","buy":"11282.8935","sale":"12470.5665"}],
    main = document.querySelector('#root');
let usd = document.createElement('div'),
    eur = document.createElement('div'),
    rur = document.createElement('div'),
    btc = document.createElement('div');
function setTextContent () {
    usd.textContent = `Валюта ${curr[0].ccy} покупка/продажа ${curr[0].buy} / ${curr[0].sale} в ${curr[0].base_ccy} `;
    eur.textContent = `Валюта ${curr[1].ccy} покупка/продажа ${curr[1].buy} / ${curr[1].sale} в ${curr[1].base_ccy} `;
    rur.textContent = `Валюта ${curr[2].ccy} покупка/продажа ${curr[2].buy} / ${curr[2].sale} в ${curr[2].base_ccy} `;
    btc.textContent = `Валюта ${curr[3].ccy} покупка/продажа ${curr[3].buy} / ${curr[3].sale} в ${curr[3].base_ccy} `;
}
function showMain (){
    main.append(usd);
    main.append(eur);
    main.append(rur);
    main.append(btc);
}
function setStyle (){
    usd.classList.add('note');
    eur.classList.add('note');
    rur.classList.add('note');
    btc.classList.add('note');
}
function start (){
    setStyle();
    setTextContent();
    showMain();
}

start();






