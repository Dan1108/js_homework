function CreateNewUser () {
    let firstName = prompt('Введите свое имя :'),
        lastName = prompt('Введите свою фамилию :');
    this.firstName = firstName;
    this.lastName = lastName;
}
CreateNewUser.prototype.getLogin = function () {
    console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
}
CreateNewUser.prototype.getBirthday = function () {
    let birthday = prompt('Введите свою дату, месяц и год рождения в формате dd.mm.yyyy');
    this.birthday = birthday;
}
CreateNewUser.prototype.getAge = function () {
    return 2020 - +this.birthday.slice(6);
}
CreateNewUser.prototype.getPassword = function () {
    console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase() + +this.birthday.slice(6));
}
CreateNewUser.prototype.filterBy = function (arr, dataType) {
    const newArr = arr.map((item) => {
        if(typeof item !== dataType) {
            return item;
        }
    })
    return newArr;
} 
function startWork() {const newUser = new CreateNewUser(); //Функция запускающая все методы созданого объекта
    newUser.getLogin();
    newUser.getBirthday();
    newUser.getPassword();
    newUser.filterBy(['Должность', 'не', 'будет', 'получена', null, 27, 26], 'string');
    console.log(newUser.getAge());
}
startWork();


